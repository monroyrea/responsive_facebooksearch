var app = angular.module('tableApp', ['ngAnimate']);
app.controller("fbCtrl",function($scope){
    $scope.share = function(nameS, imS){
        $scope.progress;
        FB.ui(
            {
                method: 'feed',
                name: nameS,
                link: window.location.href,
                picture: imS,
                caption: 'FB SEARCH FROM USC CSCI571',
            }, function(response){
                if (response && !response.error_message)
                    alert("Posted Successfully");
                else 
                    alert("Not Posted");
            });
    };
});
app.controller('datacontroller',function($scope){
    $scope.myFunc = function(){

        $scope.favs = [];
        for (var i = 0; i < localStorage.length; i++){
            var Robj = JSON.parse(localStorage.getItem(localStorage.key(i)));
            $scope.favs.push(Robj);
        }
        $scope.lat = 0;
        $scope.lon = 0;
        $scope.myPicture = [];
        $scope.counter = 0;

        $scope.hello;
        if($("#text").val() != "")
        {
            $scope.ShowMe1 = true;
            $scope.ShowMe2 = false;
            $("#text").removeAttr("data-toggle");
            $("#text").popover('hide');
            $scope.setFlag = 1;
            $scope.setKeyword = $("#text").val();
            if($("#users").hasClass("active")){
                $scope.progress = 1;
                var keywordValue = $("#text").val();
                var mainObj;
                $.get("index.php",{funcName: "searchUsers", keyword: keywordValue}, function( data ) {
                    mainObj = JSON.parse(data);
                    $scope.users = mainObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if($("#pages").hasClass("active")){
                $scope.progress = 1;
                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchPages", keyword: keywordValue}, function( data ) {
                    mainObj = JSON.parse(data);
                    $scope.pages = mainObj;
                    $scope.$digest();
                });
            }
            else if($("#events").hasClass("active")){
                $scope.progress = 1;
                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchEvents", keyword: keywordValue}, function( data ) {
                    mainObj = JSON.parse(data);
                    $scope.events = mainObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if($("#places").hasClass("active")){
                $scope.progress = 1;

                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchPlaces", keyword: keywordValue, latitude: $scope.lat, longitude: $scope.lon}, function( data ) {
                    mainObj = JSON.parse(data);
                    $scope.places = mainObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if($("#groups").hasClass("active")){
                $scope.progress = 1;

                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchGroups", keyword: keywordValue}, function( data ) {
                    mainObj = JSON.parse(data);
                    $scope.groups = mainObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
        }else{
            $("#text").attr("data-toggle", "popover");
            $("#text").popover('show');
        }
    }

    $scope.tabFunc = function(number){
        if($scope.setFlag == 1){

            $scope.ShowMe1 = true;
            $scope.ShowMe2 = false;
            if(number == 1){
                $scope.progress = 1;

                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchUsers", keyword:  $scope.setKeyword }, function( data ) {
                    var JsonObj = JSON.parse(data);
                    $scope.users = JsonObj; 
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if(number == 2){
                $scope.progress = 1;
                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchPages", keyword:  $scope.setKeyword }, function( data ) {
                    var JsonObj = JSON.parse(data);
                    $scope.pages = JsonObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if(number == 3){
                $scope.progress = 1;
                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchEvents", keyword:  $scope.setKeyword }, function( data ) {
                    var JsonObj = JSON.parse(data);
                    $scope.events = JsonObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if(number == 4){
                $scope.progress = 1;

                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchPlaces",  keyword: $scope.setKeyword, latitude: $scope.lat, longitude: $scope.lon}, function( data ) {
                    var JsonObj = JSON.parse(data);
                    $scope.places = JsonObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
            else if(number == 5){
                $scope.progress = 1;

                var keywordValue = $("#text").val();
                $.get("index.php",{funcName: "searchGroups", keyword:  $scope.setKeyword }, function( data ) {
                    var JsonObj = JSON.parse(data);
                    $scope.groups = JsonObj;
                    $scope.progress = 0;
                    $scope.$digest();
                });
            }
        }
    }

    $scope.paging = function(data,number){
        if(number ==1){
            $.get("index.php",{funcName: "paging", page: data}, function( data ) {
                mainObj = JSON.parse(data);
                $scope.users = mainObj;
                $scope.$digest();
            });
        }
        if(number ==2){
            $.get("index.php",{funcName: "paging", page: data}, function( data ) {
                mainObj = JSON.parse(data);
                $scope.pages = mainObj;
                $scope.$digest();
            });
        }
        if(number ==3){
            $.get("index.php",{funcName: "paging", page: data}, function( data ) {
                mainObj = JSON.parse(data);
                $scope.events = mainObj;
                $scope.$digest();
            });
        }
        if(number ==4){
            $.get("index.php",{funcName: "paging", page: data}, function( data ) {
                mainObj = JSON.parse(data);
                $scope.places = mainObj;
                $scope.$digest();
            });
        }
        if(number ==5){
            $.get("index.php",{funcName: "paging", page: data}, function( data ) {
                mainObj = JSON.parse(data);
                $scope.groups = mainObj;
                $scope.$digest();
            });
        }
    }

    $scope.details = function(caller,number, url, user){
        $scope.ShowMe1 = false;
        $scope.ShowMe2 = true;
        $scope.progress = 1;
        if(caller ==1){
            $.get("index.php",{funcName: "details", id: number }, 
                  function( data ) {
                var JsonObj = JSON.parse(data);
                $scope.UsersID = number.toString();
                $scope.UsersAlbums = JsonObj;
                $scope.UsersPostUser = user;
                $scope.UsersPostPicture = url;
                $scope.progress = 0;
                $scope.$digest();
            })                  
        }                

        if(caller ==2){
            $.get("index.php",{funcName: "details", id: number }, 
                  function( data ) {
                var JsonObj = JSON.parse(data);
                $scope.PagesID = number.toString();
                $scope.PagesAlbums = JsonObj;
                $scope.PagesPostUser = user;
                $scope.PagesPostPicture = url;
                $scope.progress = 0;
                $scope.$digest();
            });
        }
        if(caller ==3){
            $.get("index.php",{funcName: "details", id: number }, 
                  function( data ) {
                var JsonObj = JSON.parse(data);
                $scope.EventsID = number.toString();
                $scope.EventsAlbums = JsonObj;
                $scope.EventsPostUser = user;
                $scope.EventsPostPicture = url;
                $scope.progress = 0;
                $scope.$digest();
            });
        }
        if(caller ==4){
            $.get("index.php",{funcName: "details", id: number }, 
                  function( data ) {
                var JsonObj = JSON.parse(data);
                $scope.PlacesID = number.toString();
                $scope.PlacesAlbums = JsonObj;
                $scope.PlacesPostUser = user;
                $scope.PlacesPostPicture = url;
                $scope.progress = 0;
                $scope.$digest();
            });
        }
        if(caller ==5){
            $.get("index.php",{funcName: "details", id: number }, 
                  function( data ) {
                var JsonObj = JSON.parse(data);
                $scope.Groups = number.toString();
                $scope.GroupsAlbums = JsonObj;
                $scope.GroupsPostUser = user;
                $scope.GroupsPostPicture = url;
                $scope.progress = 0;
                $scope.$digest();
            });
        }
    }

    $scope.clearFunc = function(){
        $scope.myPicture.length = 0;
        $scope.ShowMe1 = false;
        $scope.ShowMe2 = false;
        $("#text").val("");
        $scope.setFlag = 0;
        $scope.setKeyword = "";
        $scope.users = "";
        $scope.pages = "";
        $scope.events = "";
        $scope.places = "";
        $scope.groups = "";
        $scope.UsersAlbums = ""; $scope.UsersID = undefined; $scope.UsersPostUser = "";$scope.UsersPostPicture = "";
        $scope.PagesAlbums = ""; $scope.PagesID = undefined; $scope.PagesPostUser = "";$scope.PagesPostPicture = "";
        $scope.EventsAlbums = ""; $scope.EventsID = undefined; $scope.EventsPostUser = "";$scope.EventsPostPicture = "";
        $scope.PlacesAlbums = ""; $scope.PlacesID = undefined; $scope.PlacesPostUser = "";$scope.PlacesPostPicture = "";
        $scope.GroupsAlbums = ""; $scope.GroupsID= undefined; $scope.GroupsPostUser = "";$scope.GroupsPostPicture = "";
        $scope.progress = 0;
        $scope.$digest();
    }

    $scope.back = function()
    {
        $scope.UsersID = undefined; 
        $scope.PagesID = undefined;
        $scope.EventsID = undefined;
        $scope.PlacesID = undefined;
        $scope.GroupsID= undefined;
        $scope.ShowMe2 = false;
        $scope.ShowMe1 = true;
        $scope.myPicture = [];
        $scope.progress = 0;
        $scope.$digest();
    }

    $scope.datos = function(number)
    {
        $.get("index.php",{funcName: "picture", id: number }, 
              function( data ) {
            var JsonObj = JSON.parse(data);
            JsonObj = JsonObj.data.url;
            $scope.myPicture[number] = JsonObj;

        }); 
    }

    $scope.favorites = function(id, img, name, type)
    {
        var names = {'idFav':id,'imgFav':img,'nameFav':name, 'type': type};
        localStorage.setItem(id, JSON.stringify(names));


        $scope.favs.push(names);
    }

    $scope.checkStorage = function checkStorage(id)
    {
        var a= localStorage.getItem(id); 
        var Robj = JSON.parse(a);
        return Robj.idFav;
    }

    $scope.trash = function(x, id){
        $scope.favs.splice(x,1 );
        localStorage.removeItem(id);
    } 

    $scope.change = function(Number){
        $scope.hello = !$scope.hello;
    }
});
